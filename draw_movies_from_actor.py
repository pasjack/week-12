from turtle import *
import pprint
import json
import requests


api_key = "0da8fcc02cc9349539fc45ca7800aa1c"
##### Useful URLS
# Base URL for accessing the TMBD API
movies_base= "https://api.themoviedb.org/3/"

# URL for getting information about The Avengers movie (movie id 24428)


# Additional URLS for searching
people_search = movies_base + "search/person"
movie_search = movies_base + "search/movie"

# URL for getting the cast and crew lists for movie 24428 (Avengers)
choice = input("Do you want to find movie or an actor: ")

if choice != "movie":
    ##### Code for accessing TMBD
    user_input= input("input the name of an actor/actress: ")

    # Request information about the Avengers, and pass the api_key as a parameter
    parameter = {"api_key": api_key, "query": user_input}
    result_json = requests.get(people_search, parameter)

    # Convert the results from JSON to a dictionary
    results = json.loads(result_json.text)
    pprint.pprint(results)


    # Pretty print the dictionary so we can see what it looks like
    movies = []


    for i in results['results'][0]["known_for"]:
        movies.append(i["title"])


    def draw_actor(user_input):
        write(user_input, font = ("Arial", 50, "bold"))
        right(90)
        forward(20)


    def draw_movies(movies):
        
        write("Appears in:", font = ("Arial", 20, "normal"))
        forward(25)
        left(90)
        
        size = 20
        for movie in movies:
            write(movie, font = ("Arial", size, "italic"))
            right(90)
            forward(size)
            left(90)
            if size - 2 <= 5:
                size = 5
            else:
                size -= 2


    speed(0)
    hideturtle()
    penup()

    draw_actor(user_input)


    draw_movies(movies)
    done()
else:

    ##### Code for accessing TMBD
    user_input= input("input the name of a movie: ")

    # Request information about the Avengers, and pass the api_key as a parameter
    parameter = {"api_key": api_key, "query": user_input}
    result_json = requests.get(movie_search, parameter)

    # Convert the results from JSON to a dictionary
    results = json.loads(result_json.text)

    # Pretty print the dictionary so we can see what it looks like
    cast= []

    for i in results['results'][0:1]:
        print(i['title'])
        title = i['title']

    movie_id=(i['id'])
    movie_id = str(movie_id)


    movie_credits2 = movies_base + "movie/" + movie_id +"/credits"
    result_json3 = requests.get(movie_credits2, parameter)
    results3= json.loads(result_json3.text)
    print("Starring:")
    for i in results3['cast'] [0:5]:
        print(i['name'])
        cast.append(i['name'])

    def draw_title(title):
        write(title, font = ("Arial", 50, "bold"))
        right(90)
        forward(20)


    def draw_actors(actors_list):
        
        write("Starring:", font = ("Arial", 20, "normal"))
        forward(25)
        left(90)
        
        size = 20
        for actor in actors_list:
            write(actor, font = ("Arial", size, "italic"))
            right(90)
            forward(size)
            left(90)
            if size - 2 <= 5:
                size = 5
            else:
                size -= 2


    speed(0)
    hideturtle()
    penup()


    #title = "The Avengers"
    draw_title(title)
    actor_list = [1,2,3,4,5]
    draw_actors(cast)
    done()