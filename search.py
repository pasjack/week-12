# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 15:41:34 2022

@author: pasjo
"""

api_key = "0da8fcc02cc9349539fc45ca7800aa1c"

import requests
import json
import pprint


##### Useful URLS
# Base URL for accessing the TMBD API
movies_base= "https://api.themoviedb.org/3/"

# URL for getting information about The Avengers movie (movie id 24428)


# Additional URLS for searching
people_search = movies_base + "search/person"
movie_search = movies_base + "search/movie"

# URL for getting the cast and crew lists for movie 24428 (Avengers)



##### Code for accessing TMBD
user_input= input("input the name of a movie: ")

# Request information about the Avengers, and pass the api_key as a parameter
parameter = {"api_key": api_key, "query": user_input}
result_json = requests.get(movie_search, parameter)


# Convert the results from JSON to a dictionary
results = json.loads(result_json.text)

# Pretty print the dictionary so we can see what it looks like
title = []
cast= []

for i in results['results'][0:1]:
    print(i['title'])
    title.append(i['title'])
genres=[]
movie_id=(i['id'])
movie_id = str(movie_id)

movie_credits = movies_base + "movie/" + movie_id +"credits"
result_json2 = requests.get(movie_credits, parameter)
results2 = json.loads(result_json2.text)
movie_credits2 = movies_base + "movie/" + movie_id +"/credits"
result_json3 = requests.get(movie_credits2, parameter)
results3= json.loads(result_json3.text)
for i in results2['genres']:
    print(i['name'])
    genres.append(i['name'])

print("Starring:")
for i in results3['cast'] [0:5]:
    print(i['name'])
    cast.append(i['name'])

overview =[]
overview.append(results2['overview'])
tagline =[]
tagline.append(results2['tagline'])
print(tagline)
print(overview)
#for i in results2['results']:
 #   print(results2['tagline'])
pprint.pprint(results2['overview'])
pprint.pprint(results2['tagline'])


    

