import sys
import random

def run_prog(file):
    with open(file, "r", encoding = 'utf-8') as f:
        line = "a"
        stop_list = ['the', 'and', 'I', 'as', 'for']
        i = 1
        total_words = 0
        words_dict = {}
        dic2 = {}
        while True:
            line = f.readline()
            if line == "":
                break
            line = line.replace("\n", "")
            words_list = line.split()
            for word in words_list:
                if word not in stop_list:
                    word = word.lower()
                    words_dict.setdefault(word, 0)
                    words_dict[word] += 1
            total_words += len(words_list)
            
            i += 1
       
        #for i in sorted(words_dict, key = words_dict.get, reverse = True):
        #    print(i, words_dict[i])
        #    dic2[i] = words_dict[i]
        #return dic2
        return words_dict
        

from turtle import *


def draw_words(D):
    #bgcolor("black")
    #color("lightgreen")
    goto(-450, 450)
    x = 10
    speed(0)
    i = 0
    j = 0
    #char_list = [0,1,2,3,4,5,6,7,8,9,"/", "\\", "<", ">", "[", "]", "{", "}", "@", "#", "$", "(", "-", "+"]
    freq = 10
    x_list = [0, -100, 100, -200, 200, -300, 300, -600, 400, 500, 600, -700, 700, -800]
    y_list = [100, 200, 300]
    for word, freq in D.items():
        #char = random.choice(char_list)
        if freq > 50:
           freq = 25
           x = 10
           #color("red")
        
        elif freq > 40:
           freq = 20
           x = 12
           #color("green")

        elif freq > 30:
           freq = 15
           x = 15
           #color("purple")

        elif freq  > 20:
           freq = 10
           x = 20
           #color("pink")

        elif freq > 10:
           freq = 5
           x = 40
           #color("orange")

        else:
           freq = 5
           #color("blue")

        if i == 75:
            try:
                x_choice = random.choice(x_list)
                x_list.remove(x_choice)
                y_choice = random.choice(y_list)
                #y_list.remove(y_choice)

            except IndexError:
                done()
            goto(x_choice + 2, y_choice)
            right(90)
            forward(freq)
            write(word, font = ("Arial", freq , 'normal'))
            left(90)
            i = 0
            j +=1

        else:
            right(90)
            forward(freq)
            write(word, font = ("Arial", freq , 'normal'))
            left(90)
            i += 1

    done()

dic2 = run_prog('long.txt')
hideturtle()
penup()
draw_words(dic2)

done()


